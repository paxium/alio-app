import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CheckTimesPage } from './check-times';

@NgModule({
  declarations: [
    CheckTimesPage,
  ],
  imports: [
    IonicPageModule.forChild(CheckTimesPage),
  ],
})
export class CheckTimesPageModule {}
