import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, private afd: AngularFireDatabase) {
this.getData()
  }
  getData() {
    this.afd.list('/BusTimes/9099/lat/').valueChanges().subscribe(
      data => {
        console.log(JSON.stringify(data))
        this.items = data;
    })
}

// Get Times button event
clickToTimes() {
    }
}
