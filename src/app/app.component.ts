import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';

import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public geolocation: Geolocation,
    public storage: Storage,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen) {

    this.initializeApp();
    this.geolocation.getCurrentPosition({
      enableHighAccuracy: true}
    ).then((resp) => {
      console.log('Got it', location, resp)

      let watch = this.geolocation.watchPosition();
      watch.subscribe((data) => {
      storage.set('device_lat', data.coords.latitude)
        .then(
          () => console.log('Stored item!'),
          error => console.error('Error storing item', error)
        );
      storage.set('device_lon', data.coords.longitude)
        .then(
          () => console.log('Stored item!'),
          error => console.error('Error storing item', error)
        );

      });
     // resp.coords.latitude
     // resp.coords.longitude
    }).catch((error) => {
      console.log('Error getting location', error);
    });

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
