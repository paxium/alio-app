import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { AngularFireDatabase } from 'angularfire2/database';

/*
  Generated class for the DataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataServiceProvider {

private url: string = "https://alio-4f456.firebaseio.com/BusTimes.json";

  constructor(private afd: AngularFireDatabase) {
  }
    }
